//
//  FriendsListCell.swift
//  snappy
//
//  Created by OSX on 10/16/19.
//  Copyright © 2019 eslam. All rights reserved.
//

import UIKit
import Kingfisher

class FriendsListCell: UITableViewCell {

    
    @IBOutlet weak var userVector: UIImageView!    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var lastMessage: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

     func configure (friend : NSDictionary , url : String){
        
        if  let Url = URL(string: url) {
            userVector.kf.setImage(with: Url, placeholder: UIImage(named: "user"))
        }
        userName.text = friend.object(forKey: "name") as? String
        
    }
    
    
}
