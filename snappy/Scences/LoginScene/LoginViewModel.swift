//
//  LoginViewModel.swift
//  snappy
//
//  Created by OSX on 10/15/19.
//  Copyright © 2019 eslam. All rights reserved.
//

import Foundation
import SCSDKLoginKit

class LoginViewModel {
    
    var user :UserLoginViewModel?
    var loginVC : LoginViewController?
    
    init() {
    }
    
    func loginUsingSnapchat(completion: @escaping ( _ error :  Error?) -> () ) {
        
        SCSDKLoginClient.login(from: loginVC! , completion: { success, error in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            
            if success {
                self.fetchSnapUserInfo(completion: completion) //example code
            }
        })
    }
    
    private func fetchSnapUserInfo(completion: @escaping ( _ error :  Error?) -> () ){
        let graphQLQuery = "{me{displayName, bitmoji{avatar}}}"
        SCSDKLoginClient
            .fetchUserData(
                withQuery: graphQLQuery,
                variables: nil,
                success: { userInfo in
                    if let userInfo = userInfo {
                        let data = try? JSONSerialization.data(withJSONObject: userInfo, options: .prettyPrinted)
                        if   let userEntity = try? JSONDecoder().decode(UserEntity.self, from: data!) {
                            
                            DispatchQueue.main.async {
                                self.user =  UserLoginViewModel(user: userEntity)
                                LocalStore.saveName(name: (self.user?.displayName)!)
                                LocalStore.saveVector(vector: (self.user?.avatar))
                                let firebaseObject = FirebaseManager(viewController: self.loginVC!)
                                firebaseObject.registerUserWith(name: (self.user?.displayName)!)
                                completion(nil)

                            }
                        }
                    }
            }) { (error, isUserLoggedOut) in
                completion(error)
                print(error?.localizedDescription ?? "")
        }
    }
    
    
}


class UserLoginViewModel {
    
    let displayName: String?
    let avatar: String?
    
    init(user : UserEntity) {
        
        self.displayName = user.displayName
        self.avatar = user.avatar
        
    }
    
}
