//
//  ViewController.swift
//  snappy
//
//  Created by OSX on 10/14/19.
//  Copyright © 2019 eslam. All rights reserved.
//

import UIKit

class LoginViewController : UIViewController {

    let loginViewModel = LoginViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loginViewModel.loginVC = self
        // Do any additional setup after loading the view.
    }

    @IBAction func loginButtonTapped(_ sender: Any) {
        loginViewModel.loginUsingSnapchat { (error) in
            if error == nil {

            }
            else {
                
            }
        }
    }
}

