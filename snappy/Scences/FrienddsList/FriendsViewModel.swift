//
//  FriendsViewModel.swift
//  snappy
//
//  Created by OSX on 10/16/19.
//  Copyright © 2019 eslam. All rights reserved.
//

import Foundation
import Firebase

class FriendsViewModel {
    
    var friendsListViewController : FriendsListViewController?
    var firebaseObject = FirebaseManager(viewController: nil)
    var friends = [NSDictionary]()
    var userVectors = [String]()
    
    init() {
        getAllFriends ()
    }
    
    
    private func getAllFriends (){

        firebaseObject.getAllUsers { (users) in
            self.friendsListViewController!.showSpinner(onView: self.friendsListViewController!.view)
            var count = 0
            for user in users! {
                count += 1
                self.friends.append(user as! NSDictionary)
                self.getUserImages(dict: user as! NSDictionary, Completion: ({
                    
                    if self.friends.count == self.userVectors.count {
                        self.friendsListViewController!.removeSpinner()
                        self.friendsListViewController?.tableView.reloadData()
                    }
                    }
                ))
            }
        }
    }
    
    private func getUserImages (dict : NSDictionary , Completion : @escaping () ->()) {
        let imageName = "imgUserProfile"
        firebaseObject.storageRootRef = Storage.storage().reference(forURL: "gs://snappy-6395f.appspot.com")
        let imagePath = dict.object(forKey: "firebaseId")
        let url = firebaseObject.storageRootRef!.child(imagePath as! String).child(imageName)
        url.downloadURL(completion: { (url, error) in
            if let url = url {
                self.userVectors.append(url.absoluteString)
            }
            else {
                self.userVectors.append("")
            }
            Completion()
        })
    }
    
    
}
