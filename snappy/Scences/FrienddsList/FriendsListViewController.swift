//
//  FriendsListViewController.swift
//  snappy
//
//  Created by OSX on 10/15/19.
//  Copyright © 2019 eslam. All rights reserved.
//

import UIKit

class FriendsListViewController: UITableViewController {
    
    let friendsViewModel = FriendsViewModel()
    override func viewDidLoad() {
        
        super.viewDidLoad()
        friendsViewModel.friendsListViewController = self

    }

    override func viewWillAppear(_ animated: Bool) {
       setupUI()
    }
    
    func setupUI()  {
        let logo = UIImage(named: "Snapfriends")
        let logoView = UIImageView(image:logo)
        logoView.contentMode = .center
        self.navigationItem.titleView = logoView
        
        let imageView  = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        imageView.image = UIImage.incodeImageData(data: LocalStore.getVector())
        let barButton = UIBarButtonItem(customView: imageView)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return friendsViewModel.friends.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FriendsListCell", for: indexPath) as! FriendsListCell
        cell.configure(friend: friendsViewModel.friends[indexPath.row], url: friendsViewModel.userVectors[indexPath.row])

        return cell
    }
 

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
        vc.dict = friendsViewModel.friends[indexPath.row]
        vc.vectorUrl = friendsViewModel.userVectors[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
        let backItem = UIBarButtonItem()
        let senderDisplayName = "\(String(describing: vc.dict.object(forKey: "name")!))"
        backItem.title = senderDisplayName
        navigationItem.backBarButtonItem = backItem 
    }

}
