//
//  FirebaseHandler.swift
//  snappy
//
//  Created by OSX on 10/16/19.
//  Copyright © 2019 eslam. All rights reserved.
//

import Foundation
import Firebase
import FirebaseStorage
import FirebaseDatabase
import FirebaseAuth

class FirebaseManager {
    
    var uid: String?
    var databaseRootRef: DatabaseReference?
    var storageRootRef: StorageReference?
    var authentication: Auth?
    var viewController : UIViewController?
    init(viewController : UIViewController?) {
        self.viewController = viewController
        initialize()
    }
    
    func initialize() {
        uid = Auth.auth().currentUser?.uid
        databaseRootRef = Database.database().reference()
        storageRootRef = Storage.storage().reference()
        authentication = Auth.auth()
    }
    
    func isAuthenticated() -> Bool {
        return authentication?.currentUser != nil
    }
    
    func registerUserWith(name: String) {
        Auth.auth().createUser(withEmail: "jpt\(String(describing: name.first!))@ss.com", password: name) { (res, error) in
            
            if error == nil {

                let filePath = "\(Auth.auth().currentUser!.uid)/\("imgUserProfile")"
                let metaData = StorageMetadata()
                metaData.contentType = "image/jpg"
                self.storageRootRef!.child(filePath).putData(LocalStore.getVector()!, metadata: metaData){(metaData,error) in
                    if let error = error {
                        print(error.localizedDescription)
                        return
                    }
                }
                let name = ["Name": name]
       self.databaseRootRef!.child("users").child((res?.user.uid)!).setValue(["username": name])
              let appDelegate = UIApplication.shared.delegate as! AppDelegate
              appDelegate.firstLaunch()
            } else {
                let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                alertController.addAction(defaultAction)
                self.viewController!.present(alertController, animated: true, completion: nil)
            }
        }
        
    }
    
    
    func getAllUsers(completion : @escaping ( _ UserList :  NSMutableArray?) -> ())  {
        let arrUserList = NSMutableArray()
        databaseRootRef!.child("users").observeSingleEvent(of: .value, with:  { (snapshot) in
            if snapshot.exists()
            {
                if let fruitPost = snapshot.value as? Dictionary<String,AnyObject>
                {
                    for(key, value) in fruitPost {
                        if let fruitData = value as? Dictionary<String,AnyObject> {
                            
                            if(Auth.auth().currentUser!.uid != key){
                                let dict = NSMutableDictionary()
                                dict.setObject(key, forKey:"firebaseId" as NSCopying)
                                dict.setObject((fruitData["username"] as! NSDictionary).value(forKey: "Name")!, forKey: "name" as NSCopying)
                                arrUserList.add(dict)
                            }
                        }
                    }
                    completion(arrUserList)
                }
                print(arrUserList)
            }
        })
    }
}
