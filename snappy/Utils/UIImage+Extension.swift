//
//  File.swift
//  SnapClient
//
//  Created by Kei Fujikawa on 2018/07/09.
//  Copyright © 2018年 Kboy. All rights reserved.
//

import UIKit

extension UIImageView {
    
    func load(from urlString: String){
        guard let imageURL = URL(string: urlString) else {
            return
        }
        
        do {
            let data = try Data(contentsOf: imageURL)
            let image = UIImage(data: data)
            self.image = image
        } catch {
            
        }
    }
}

extension UIImage {
    
    static func load(from urlString: String) -> Data? {
        guard let imageURL = URL(string: urlString),
            let data = try? Data(contentsOf: imageURL) else {
                return nil
        }
        return data
    }
    
    static func incodeImageData (data : Data?) -> UIImage {
        
        if let _ = data {
            let image =  UIImage(data: data!)!
            let compressedImage = image.resizeWithWidth(width: 30)
            return compressedImage!
        }
        else {
            return UIImage(named: "user")!
        }
    }
    
    func resizeWithWidth(width: CGFloat) -> UIImage? {
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))))
        imageView.contentMode = .scaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return result
    }
}
