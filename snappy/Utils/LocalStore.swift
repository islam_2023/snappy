//
//  LocalStore.swift
//  snappy
//
//  Created by OSX on 10/15/19.
//  Copyright © 2019 eslam. All rights reserved.
//

import Foundation
import UIKit

class LocalStore {
    
    class func getName() -> String?
    {
        return UserDefaults.standard.string(forKey: "name")
    }
    
    class func saveName(name:String)
    {
        UserDefaults.standard.setValue(name, forKey: "name")
    }
    class func getVector() -> Data?
    {
        return UserDefaults.standard.data(forKey: "vector")
    }
    
    class func saveVector(vector:String?)
    {
        let data =  UIImage.load(from: vector ?? "")
        UserDefaults.standard.setValue(data, forKey: "vector")
    }
}
